.PRECIOUS: %.cmi %.cmo

# all:
# 	ocamlbuild -use-ocamlfind -package graphics phone.byte

# clean:
# 	ocamlbuild -clean

all : tp6.ml
	ocamlc -o a.out graphics.cma tp6.ml


clean :
	rm -rf *.cmi
	rm -rf *.cmo
	rm -rf a.out
