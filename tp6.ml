type grille = bool array array 

let coeff = 10

(* Question 1 *)
let creation n = 
    Array.init n ( fun i -> Array.make n false )

(* Question 2 *)
let init_centre g =
    let i = (Array.length g)/2 in
    Array.set g.(i) i true

let init_aleatoire g p =
    let rec aux i j g n =
        if ( i < n ) then (
            if ( j >= n ) then (
                aux (i+1) 0 g n
            )
            else (
                let r = Random.int 100 in
                if (r < p) then (
                    g.(i).(j) <- true;
                )
                else (
                    g.(i).(j) <- false;
                );
                aux i (j+1) g n 
            )
        )
    in aux 0 0 g (Array.length g)
            

let () = Random.self_init ()

(* Question 3 *)
open Graphics

let affichage g = 
    let rec aux i j g n =
         if ( i < n ) then (
            if ( j >= n ) then (
                aux (i+1) 0 g n
            )
            else (
                if g.(i).(j) then (
                    set_color black
                )
                else (
                    set_color white
                );
                fill_rect (i*coeff) (j*coeff) coeff coeff;
                aux i (j+1) g n
            )
        )
    in aux 0 0 g (Array.length g); synchronize ()

let print_matrix g =
    Array.iter (fun x ->
        Array.iter (fun y -> Printf.printf "%B " y ) x; Printf.printf "\n" ) g

(* Question 4 *)
type etat = { up : bool; down: bool; right : bool; left : bool }

let prend g i j n =
         if ( i < n && j < n && i >= 0 && j >= 0 ) then (
            g.(i).(j)
        ) else (
            false
        )

let voisines g i j =
    let len = Array.length g in
    {
        up = prend g (i) (j+1) len;
        down = prend g (i) (j-1) len;
        right = prend g (i+1) (j) len;
        left = prend g (i-1) (j) len
    }

(* Question 6 *)
let int_of_bool b =
    if b then 1 else 0

let fredkin g i j =
    let v = voisines g i j in
    let s =
        int_of_bool v.up +
        int_of_bool v.down +
        int_of_bool v.right +
        int_of_bool v.left 
    in s mod 2 != 0
    
(* Question 7 *)
let reaction g1 g2 =
    affichage g1;
    let rec aux g1 g2 i j n =
        if ( i < n ) then (
            if ( j >= n ) then (
                aux g1 g2 (i+1) 0 n
            )
            else (
                g2.(i).(j) <- fredkin g1 i j;
                aux g1 g2 i (j+1) n
            )
        )       
    in aux g1 g2 0 0 (Array.length g2)

(* Question 7 *)
let rec boucle g1 g2 =
    reaction g1 g2;
    boucle g2 g1

(* Question 4 *)
type etatbis = { 
    up : bool; 
    up_left : bool;
    up_right : bool;
    down: bool; 
    down_left : bool;
    down_right : bool;
    right : bool; 
    left : bool 
  }

let voisinesbis g i j =
    let len = Array.length g in
    {
        up = prend g (i) (j+1) len;
        down = prend g (i) (j-1) len;
        right = prend g (i+1) (j) len;
        left = prend g (i-1) (j) len;
        up_left = prend g (i+1) (j-1) len;
        up_right = prend g (i+1) (j+1) len;
        down_left = prend g (i-1) (j-1) len;
        down_right = prend g (i-1) (j+1) len;
    }

let jeu_vie g i j =
    let v = voisinesbis g i j in
    let s =
        int_of_bool v.up +
        int_of_bool v.down +
        int_of_bool v.right +
        int_of_bool v.left +
        int_of_bool v.up_left +
        int_of_bool v.up_right +
        int_of_bool v.down_left +
        int_of_bool v.down_right
    in
    if g.(i).(j) then
        2 = s || 3 = s
    else
        s = 3

let reaction_vie g1 g2 =
    affichage g1;
    let rec aux g1 g2 i j n =
        if ( i < n ) then (
            if ( j >= n ) then (
                aux g1 g2 (i+1) 0 n
            )
            else (
                g2.(i).(j) <- jeu_vie g1 i j;
                aux g1 g2 i (j+1) n
            )
        )       
    in aux g1 g2 0 0 (Array.length g2)

let rec boucle_vie g1 g2 =
    reaction_vie g1 g2;
    boucle_vie g2 g1 

let () =
    let g = creation 200 in
    let len = Array.length g in
    let len_str = string_of_int (len*coeff) in
    open_graph (" " ^ len_str ^ "x" ^ len_str);
    auto_synchronize false;
    init_aleatoire g 15;
    (* init_centre g; *)
    boucle_vie g (creation (Array.length g));
    (* boucle g (creation (Array.length g)); *)
    ignore (read_key ())
